; vim:syntax=asm:
; using 282 as prefix for local labels

#pragma once

; This file contains 16 bit operations

; it is not necessary to have a TMP register!

#include "internal.h"

;;;.macro Cpi16 nHigh nLow m
;;;	cpi	\nHigh, hi8(\m)
;;;	brne	0f	; if not equal comparing high is enough
;;;	cpi	\nLow, lo8(\m)
;;;0:	; end Cpi16
;;;.endm

.macro Cpi16 RdnHigh RdnLow k
  INFO("Cpi16")
  IfLtR16 \RdnHigh, LTr16_RdnHigh
  IfLtR16 \RdnLow, LTr16_RdnLow
  .if LTr16_RdnHigh || LTr16_RdnLow
    .error "Ldi16: RdnHigh or RdnLow < r16"
  .else
	cpi	\RdnHigh, hi8(\k)
	brne	282050f	; if not equal comparing high is enough
	cpi	\RdnLow, lo8(\k)
282050:	; end Cpi16
  .endif
.endm


.macro XORSwap Rdx Rdy
  INFO("XORSwap")
  .ifnc \Rdx, \Rdy
	eor	\Rdx, \Rdy
	eor	\Rdy, \Rdx
	eor	\Rdx, \Rdy
  .endif
.endm

.macro Cp16 RdnHigh RdnLow RdmHigh RdmLow
	INFO("Cp16")
	cp	\RdnLow, \RdmLow
	cpc	\RdnHigh, \RdmHigh
.endm

.macro Ldi16 RdnHigh RdnLow k RdTmp=r0 WarnIfSlow=0
  INFO("Ldi16")
  IfLtR16 \RdnHigh, LTr16_RdnHigh
  IfLtR16 \RdnLow, LTr16_RdnLow
  IfLtR16 \RdTmp, LTr16_RdTmp
  .if LTr16_RdnHigh || LTr16_RdnLow
    .if LTr16_RdTmp
	.error "Ldi16: RdnHigh or RdnLow < r16 and RdTmp invalid"
    .else
	.if \WarnIfSlow
	  .warning "Ldi16: using slower version"
	.endif
	ldi	\RdTmp, hi8(\k)
	mov	\RdnHigh, \RdTmp
	ldi	\RdTmp, lo8(\k)
	mov	\RdnLow, \RdTmp
    .endif
  .else
	ldi	\RdnHigh, hi8(\k)
	ldi	\RdnLow, lo8(\k)
  .endif
.endm

.macro Tst16 RdnHigh RdnLow
	INFO("Tst16")
	tst	\RdnHigh
	brne	282100f	; if not 0 can use nHigh
	tst	\RdnLow
282100:	; end Tst16
.endm

.macro Sub16 RdxHigh RdxLow RdyHigh RdyLow	; x - y
	INFO("Sub16")
	sub	\RdxLow, \RdyLow
	sbc	\RdxHigh, \RdyHigh
.endm

.macro Add16 RdxHigh RdxLow RdyHigh RdyLow	; x + y
	INFO("Add16")
	add	\RdxLow, \RdyLow
	adc	\RdxHigh, \RdyHigh
.endm

.macro Addi16 RdnHigh RdnLow k RdTmp=r0 WarnIfSlow=0	; x + k
  INFO("Addi16")
  IfLtR16 \RdnHigh, LTr16_RdnHigh
  IfLtR16 \RdnLow, LTr16_RdnLow
  IfLtR16 \RdTmp, LTr16_RdTmp
  .if LTr16_RdnHigh || LTr16_RdnLow
    .if LTr16_RdTmp
	.error "Addi16: RdnHigh or RdnLow < r16 and RdTmp invalid"
    .else
	.if \WarnIfSlow
	  .warning "Addi16: using slower version"
	.endif
	ldi	\RdTmp, lo8(\k)
	add	\RdnLow, \RdTmp
	ldi	\RdTmp, hi8(\k)
	adc	\RdnHigh, \RdTmp
    .endif
  .else
	subi	\RdnLow, lo8(-\k)
	sbci	\RdnHigh, hi8(-\k)
  .endif
.endm

; RdTmp remains unchanged (even if used)
.macro Inc16 RdnHigh RdnLow RdTmp=r0 WarnIfSlow=0
  INFO("Inc16")
  IfLtR16 \RdnHigh, LTr16_RdnHigh
  IfLtR16 \RdnLow, LTr16_RdnLow
  IfLtR16 \RdTmp, LTr16_RdTmp
  .if LTr16_RdnHigh || LTr16_RdnLow
    .if LTr16_RdTmp
	.error "Inc16: RdnHigh or RdnLow < r16 and RdTmp invalid"
    .else
	.if \WarnIfSlow
	  .warning "Inc16: using slower version"
	.endif
	XORSwap \RdTmp, \RdnLow
	subi	\RdTmp, lo8(-1)
	XORSwap \RdTmp, \RdnLow

	XORSwap \RdTmp, \RdnHigh
	sbci	\RdTmp, hi8(-1)
	XORSwap \RdTmp, \RdnHigh
    .endif
  .else
	subi	\RdnLow, lo8(-1)		; n + 1 == n - (-1)
	sbci	\RdnHigh, hi8(-1)
  .endif
.endm

; RdTmp remains unchanged (even if used)
.macro Dec16 RdnHigh RdnLow RdTmp=r0 WarnIfSlow=0
  INFO("Dec16")
  IfLtR16 \RdnHigh, LTr16_RdnHigh
  IfLtR16 \RdnLow, LTr16_RdnLow
  IfLtR16 \RdTmp, LTr16_RdTmp
  .if LTr16_RdnHigh || LTr16_RdnLow
    .if LTr16_RdTmp
	.error "Dec16: RdnHigh or RdnLow < r16 and RdTmp invalid"
    .else
	.if \WarnIfSlow
	  .warning "Dec16: using slower version"
	.endif
	XORSwap \RdTmp, \RdnLow
	subi	\RdTmp, lo8(1)
	XORSwap \RdTmp, \RdnLow

	XORSwap \RdTmp, \RdnHigh
	sbci	\RdTmp, hi8(1)
	XORSwap \RdTmp, \RdnHigh
    .endif
  .else
	subi	\RdnLow, lo8(1)
	sbci	\RdnHigh, hi8(1)
  .endif
.endm


; multiply n (nHigh<<8 + nLow) with m (mHigh<<8 + mLow)
; result is 4 bytes (res3<<24 + res2<<16 + res1<<8 + res0)
; modifies m!
.macro Mult16 RdnHigh RdnLow RdmHigh RdmLow Rdres3 Rdres2 Rdres1 Rdres0
	INFO("Mult16")
	eor	\Rdres3, \Rdres3
	eor	\Rdres2, \Rdres2
	eor	\Rdres1, \Rdres1
	eor	\Rdres0, \Rdres0

	; set res1 to 0b10000000 --> if this 1 is shifted into carry
	; we have the 17th round
	sec			; set carry
	ror	\Rdres1

282200:	;MULT16_NEXT_BIT

	; shift result left
	lsl	\Rdres0
	rol	\Rdres1
	rol	\Rdres2
	rol	\Rdres3
	
	; if carry is set we have the 17th round
	brcs	282202f		;MULT16_END

	lsl	\RdmLow
	rol	\RdmHigh

	; look at highest bit
	brcc	282201f
	
	add	\Rdres3, \RdnLow
	AddCarry \Rdres2
	AddCarry \Rdres1
	AddCarry \Rdres0

	add	\Rdres2, \RdnHigh
	AddCarry \Rdres1
	AddCarry \Rdres0
282201:
	rjmp	282200b		;MULT16_NEXT_BIT
	
282202:	;MULT16_END:
.endm

; divides n (n3<<24 + n2<<16 + n1<<8 + n0) by m (mHigh<<8 + mLow)
; result is 4 bytes (res3<<24 + .. + res0)
; modifies n!
.macro Div16 Rdn3 Rdn2 Rdn1 Rdn0 RdmHigh RdmLow Rdres3 Rdres2 Rdres1 Rdres0 RdtmpHigh RdtmpLow
	INFO("Div16")
	clr	\RdtmpHigh
	clr	\RdtmpLow

	clr	\Rdres3
	clr	\Rdres2
	clr	\Rdres1
	clr	\Rdres0

	inc	\Rdres0

282300:	; division loop
	; n*2, roling highest bit into TMP
	lsl	\Rdn0
	rol	\Rdn1
	rol	\Rdn2
	rol	\Rdn3
	rol	\RdtmpLow
	rol	\RdtmpHigh
	brcs	282301f
	Cp16 \RdtmpHigh, \RdtmpLow, \RdmHigh, \RdmLow
	brcs	282302f
282301:	; division possible
	Sub16 \RdtmpHigh, \RdtmpLow, \RdmHigh, \RdmLow
	sec	; set carry bit
	rjmp	282303f
282302:	; division not possible
282303:	; put carry into result
	lsl	\Rdres0
	rol	\Rdres1
	rol	\Rdres2
	rol	\Rdres3
	; as long as the first 1 we set (via inc) hasn't shifted out, continue:
	brcc	282300b
.endm

