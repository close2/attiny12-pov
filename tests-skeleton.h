; vim:syntax=asm:
; using 154 as prefix for local labels

; test-skeleton

#pragma once

#include "internal.h"
#include <avr/io.h>


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                                                              ;
;                              the real skeleton                               ;
;                                                                              ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

.section .text
.org 0x00
; Interrupt vectors
	rjmp	RESET		; 0 RESET
	rjmp	INT0_vect	; 1 INT0
	rjmp	IO_PINS_vect	; 2 I/O Pins
	rjmp	TIMER0_OVF0_vect; 3 TIMER0, OVF0
	rjmp	EE_RDY_vect	; 4 EE_RDY
	rjmp	ANA_COMP_vect	; 5 ANA_COMP

DATA_ENC:
;	.byte	0x00, 0x01, 0x02, 0x03
;	.byte	0x04, 0x05, 0x06, 0x07
;	.byte	0x08, 0x09, 0x0A, 0x0B
;	.byte	0x0C, 0x0D, 0x0E, 0x0F

INT0_vect:		; 1 INT0
	TESTS_IRQ_INT0

IO_PINS_vect:		; 2 I/O Pins
	TESTS_IRQ_IO_PINS

TIMER0_OVF0_vect:	; 3 TIMER0, OVF0
	TESTS_IRQ_TIMER0_OVF0

EE_RDY_vect:		; 4 EE_RDY
	TESTS_IRQ_EE_RDY

ANA_COMP_vect:		; 5 ANA_COMP
	TESTS_IRQ_ANA_COMP


RESET:
	INFO("RESET")
	TESTS_RESET

MAIN:
	INFO("MAIN")
	TESTS_MAIN

MAIN_LOOP:
	INFO("MAIN_LOOP")
	TESTS_MAIN_LOOP
	rjmp	MAIN_LOOP

.section .eeprom
	.byte	TESTS_EEPROM_DATA1
	.byte	TESTS_EEPROM_DATA2
;	.byte	0x0D
;	.byte	0x0C
;	.byte	0x0B
;	.byte	0x0A
;	.byte	0x09
;	.byte	0x08
;	.byte	0x07
;	.byte	0x06
;	.byte	0x05
;	.byte	0x04
;	.byte	0x03
;	.byte	0x02
;	.byte	0x01
;	.byte	0x00
;	.byte	0xFF

