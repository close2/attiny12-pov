; vim:syntax=asm:

#define DEBUG

#include "internal.h"

#define TMP r16
#define TMP_IRQ r17
#define RD_RESULT r0

#include "helpers.h"

#define TESTS_RESET BranchAfterResetTest

#include "tests-defines.h"

.macro BranchAfterResetTest
	INFO("BranchAfterResetTest")

	clr	RD_RESULT
	BranchAfterReset AFTER_RESET, AFTER_POWER_ON

AFTER_POWER_ON:
	com	RD_RESULT

AFTER_RESET:	
	ShowResult RD_RESULT
.endm

#include "tests-skeleton.h"

