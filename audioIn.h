; vim:syntax=asm:
; using 317 as prefix for local labels

#pragma once

#include "internal.h"
#include "helpers.h"

; audio input for attiny12
; different frequencies are used for data-transmitting

; HOWTO:
;
; define TMP (or AUDIO_IN_TMP) before including this header-file
; put AudioInIRQ_CountAnalogComp into the IRQ-routine of the AnalogCompIRQ
; call AudioInInitialize
; call AudioInDecode (the first 2 parameters must of course be the same registers
; as those specified to AudioInIRQ_CountAnalogComp)
; the eeprom should now be written.
;
; if you don't want to write the read data to the eeprom, you can
; define the following 2 C-macros:
;
; AUDIO_IN_INIT, which by default clears the Eeprom-address
; AUDIO_IN_TREAT_READ_BYTE
; default implementation:
;#  define AUDIO_IN_TREAT_READ_BYTE WriteEepromAndIncAddress \RdReadByte
;
; if the Z-flag is 1 after AUDIO_IN_TREAT_READ_BYTE, stops
; (cp with equal values)

; SENDER:
; assumes atmel runs at 1.2 MHz
; 1 startFreq during 0.5s
; 2 syncFreq during 0.5s
;   both start- and syncFreq are quite high
;   --> measuring 0.25s must be exact enough
; 3 transmit 4 bytes in 4 * 2 seconds using 4 different freqs.
;   --> every freq. "covers" 2 bits
;   (4 values / byte) --> 0.5s for every freq
; goto 1

; RECEIVER:
; 1 try (in a loop) to read startFreq. (during 0.25s)
;   continue loop read until freq is different
;   we are now between start of syncFreq and 0.25s of startFreq
; 2 try to read syncFreq. (during 0.25s)
;   we are now between 0.25s of startFreq and end of startFreq
;   average should be 0.25 + 0.25/2 = 0.375s of startFreq
; 3 sleep 0.25s (0.125 until end of startFreq + 0.125)
;   0.125 + 0.25[reading] + 0.125 = 0.5 the duration of the 
; readData:
; 4 0.25s readFreq
; 5 sleep 0.25s
; 6 goto readData until 16 values have been read
; 7 goto 1

#ifndef AUDIO_IN_TREAT_READ_BYTE
#  define AUDIO_IN_TREAT_READ_BYTE WriteEepromAndIncAddress \RdReadByte
#endif

#ifndef AUDIO_IN_INIT
#  define AUDIO_IN_INIT ClearEepromAddress
#endif

#ifndef AUDIO_IN_TMP
#  ifdef TMP
#    define AUDIO_IN_TMP TMP
#  else
#    error "You must either define the TMP or AUDIO_IN_TMP register"
#  endif
#endif

#ifndef AUDIO_EEPROM_SIZE
#  ifdef EEPROM_SIZE
#    define AUDIO_EEPROM_SIZE EEPROM_SIZE
#  else
#    error "You must either define the EEPROM_SIZE or AUDIO_EEPROM_SIZE register"
#  endif
#endif


.macro WriteEepromAndIncAddress RdByte
	INFO("WriteEepromAndIncAddress")
	WriteEeprom \RdByte
	IncEepromAddress kCompareWith=EEPROM_SIZE
.endm

.macro AudioInReadFreq RdDurationHigh RdDurationLow RdFreqHigh RdFreqLow
	INFO("AudioInReadFreq")
	clr	\RdFreqLow
	clr	\RdFreqHigh
	AnalogCompOn
	SleepMilliSecs16 \RdDurationHigh, \RdDurationLow
	AnalogCompOff
.endm

; this macro goes into the IRQ-function
; RdFreqHigh and RdFreqLow should be > r15
.macro AudioInIRQ_CountAnalogComp RdFreqHigh RdFreqLow
	INFO("AudioInIRQ_CountAnalogComp")
	; can use AUDIO_IN_TMP even though inside IRQ, because value remains unchanged
	Inc16 \RdFreqHigh, \RdFreqLow, AUDIO_IN_TMP, 1
.endm

.macro AudioInInitialize kUseIntRevV=0
	INFO("AudioInInitialize")
	.equ AudioIn_Initialized, 1
	SleepInitialize
	AnalogCompOn

      .if \kUseIntRevV >= 0
	INFO("AudioInInitialize: Using internal Referenz Voltage as non-inverted input")
	AnalogCompIntRefV
      .else
	INFO("AudioInInitialize: Using Pin AIN0 (PB0) as non-inverted input")
	AnalogCompPinComp
      .endif

	AUDIO_IN_INIT
.endm

; returns 0 if below kLowBorderFreq
; and returns RdRangeNb so that:
; kLowBorderFreq + RdRangeNb * kStep < RdReq{High,Low} <= kLowBorderFreq + (RdRangeNb + 1) * kStep
.macro AudioInFindRange RdFreqHigh RdFreqLow RdRangeNb kLowBorderFreq kStep RdTmpHigh RdTmpLow
	INFO("AudioInFindRange")
	Ldi16 \RdTmpHigh, \RdTmpLow, \kLowBorderFreq, AUDIO_IN_TMP, 1

	clr	\RdRangeNb

317100:	; loop start
	Cp16 \RdFreqHigh, \RdFreqLow, \RdTmpHigh, \RdTmpLow
	brlo	317101f	; break
	inc	\RdRangeNb
	Addi16 \RdTmpHigh, \RdTmpLow, \kStep, AUDIO_IN_TMP, 1

	rjmp	317100b

317101:	; FindRangeEND; RdFreq{High,Low} < RdTmp{High,Low}
.endm

; note RdCurrentStep has some substeps compared to the description at the top of this file
.macro AudioInDecode RdDurationHigh RdDurationLow RdFreqHigh RdFreqLow RdRangeNb kLowBorderFreq kStep RdTmpHigh RdTmpLow, kSyncFreqRangeNb, kStartFreqRangeNb, kDataFreq1RangeNb, RdCurrentStep, RdReadByte
	INFO("AudioInDecode")

	.ifndef AudioIn_Initialized
	  .error "AudioInDecode: you need to call AudioInInitialize first"
	.endif

	clr	\RdReadByte
	rjmp	317205f	; jump to first step

	; these blocks are used in multiple steps
317201:	Ldi16 \RdDurationHigh, \RdDurationLow, 250
	SleepMilliSecs16 \RdDurationHigh, \RdDurationLow
317200:	Ldi16 \RdDurationHigh, \RdDurationLow, 250
	AudioInReadFreq \RdDurationHigh, \RdDurationLow, \RdFreqHigh, \RdFreqLow
	; divide kLowBorderFreq and kStep by 4 (because only 250ms)
	AudioInFindRange \RdFreqHigh, \RdFreqLow, \RdRangeNb, \kLowBorderFreq/4, \kStep/4, \RdTmpHigh, \RdTmpLow

	ldi	AUDIO_IN_TMP, 100
	cp	\RdCurrentStep, AUDIO_IN_TMP
	breq	317211f

	ldi	AUDIO_IN_TMP, 101
	cp	\RdCurrentStep, AUDIO_IN_TMP
	breq	317221f

	ldi	AUDIO_IN_TMP, 102
	cp	\RdCurrentStep, AUDIO_IN_TMP
	breq	317231f

	; otherwise read bytes:
	rjmp	317240f
	;------------------------------------


317205:	; STEP 1
	ldi	AUDIO_IN_TMP, 100
	mov	\RdCurrentStep, AUDIO_IN_TMP

317210:	; loop try to read start-freq
	; try to read start-freq
	rjmp	317200b	; read freq and find range
317211:	ldi	AUDIO_IN_TMP, \kStartFreqRangeNb
	cpse	\RdRangeNb, AUDIO_IN_TMP	; break if RdRangeNb == kStartFreqRangeNb
	rjmp	317210b

	inc	\RdCurrentStep	; 101

317220:	; continue reading freqs until different to start-freq
	rjmp	317200b	; read freq and find range
317221:	ldi	AUDIO_IN_TMP, \kStartFreqRangeNb
	cp	\RdRangeNb, AUDIO_IN_TMP
	breq	317220b

	; STEP 2
	inc	\RdCurrentStep	; 102

317230:	; try to read syncFreq
	rjmp	317200b	; read freq and find range
317231:	ldi	AUDIO_IN_TMP, \kSyncFreqRangeNb
	cpse	\RdRangeNb, AUDIO_IN_TMP
	rjmp	317295f	; abort

	; STEP 3
	; sleep 0.175s
	Ldi16 \RdTmpHigh, \RdTmpLow, 175, AUDIO_IN_TMP
	SleepMilliSecs16 \RdTmpHigh, \RdTmpLow

	; READDATA
	; STEP 4
	; set \RdCurrentStep to 0
	clr	\RdCurrentStep

	rjmp	317200b	; read freq and find range
317240:	; verify byte read is valid
	ldi	AUDIO_IN_TMP, \kDataFreq1RangeNb
	sub	\RdRangeNb, AUDIO_IN_TMP

	ldi	AUDIO_IN_TMP, 4
	cp	\RdRangeNb, AUDIO_IN_TMP
	brsh	317295f	; error freq either too high or too low

	lsl	\RdReadByte
	lsl	\RdReadByte

	or	\RdReadByte, \RdRangeNb

	inc	\RdCurrentStep

	; now find out if byte has been finished
	mov	AUDIO_IN_TMP, \RdReadByte
	andi	AUDIO_IN_TMP, 0b00000111
	breq	317250f	; byte has been finished

	; continue reading byte
	rjmp	317201b

317250:	; byte has been finished
	inc	\RdCurrentStep

	AUDIO_IN_TREAT_READ_BYTE
	; use Z(ero)-flag set by AUDIO_IN_TREAT_READ_BYTE
	breq	317299f

	clr	\RdReadByte

	; find out, if we need to restart with step 1 (startFreq during 0.5sec)
	ldi	AUDIO_IN_TMP, 16
	cpse	AUDIO_IN_TMP, \RdCurrentStep
	rjmp	317201b	; RdCurrentStep != 16
	rjmp	317205b	; RdCurrentStep == 16

317295:	; ERROR END
317299:
	Ldi16	\RdTmpHigh, \RdTmpLow, 250
	SleepMilliSecs16 \RdTmpHigh, \RdTmpLow
.endm

