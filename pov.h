; vim:syntax=asm:
; using 193 as prefix for local labels

#pragma once

#include <avr/io.h>
#include "internal.h"
#include "helpers.h"

; POV for attiny12

; HOWTO:
;
; define TMP _before_ including this header-file
; (or POV_TMP)
;
; except for the IRQ-table (and the RD_... definitions),
; there is a complete program in this comment.
;
; You can change some default-values using C-macros (see below)
;
;
;DATA_ENC:
;	PovCharEncodings
;
;RESET:
;MAIN:
;	AnalogCompOff
;
;	PovInitialize RD_EEP_CTR, RD_COLORS
;
;	sei				; enable interrupts
;
;	; sleep 200 millisecs before starting the prog (not necessary)
;	Ldi16 RD_MILLI_SECS_HIGH, RD_MILLI_SECS_LOW, 200
; 	SleepMilliSecs16 RD_MILLI_SECS_HIGH, RD_MILLI_SECS_LOW
;
;MAIN_LOOP:
;	PovGetAndTreatChar RD_EEP_CHAR, RD_EEP_CTR, RD_PAUSE_CTR, RD_OUTPUT, RD_COLORS, RD_MEM_CHAR, RD_MILLI_SECS_HIGH, RD_MILLI_SECS_LOW
;	rjmp	MAIN_LOOP
;
; .section .eeprom
; 	.byte	(CHAR_H - DATA_ENC)/4
; 	.byte	(CHAR_E - DATA_ENC)/4
; 	.byte	(CHAR_L - DATA_ENC)/4
; 	.byte	(CHAR_L - DATA_ENC)/4
; 	.byte	(CHAR_O - DATA_ENC)/4
; 	.byte	(0b1010000 + POV_SPACE_NB_PAUSE); 1 space
; 	.byte	(CHAR_W - DATA_ENC)/4
; 	.byte	(CHAR_O - DATA_ENC)/4
; 	.byte	(CHAR_R - DATA_ENC)/4
; 	.byte	(CHAR_L - DATA_ENC)/4
; 	.byte	(CHAR_D - DATA_ENC)/4
; 	.byte	(0b10100000 + POV_SPACE_NB_PAUSE); 1 space
; 	.byte	0xFF

; up to 127 different character encodings are allowed in the POV_ENCODINGS_LOCATION
;
; every character-encoding uses exactly 4 bytes, even if possibly less bytes
; are used
; the 5 least significant bits are used to represent 1 column
; 0b000x normal
; 0b001x last column
; 0b010x add pause (between this and next column --> pause between letters)
; 0b011x last and add pause
; 0b100x add a space
; 0b101x last and add space
; 0b110x change color
; 0b111x change color, last
; POV_ENCODINGS_LOCATION _MUST_ stay < 0xFF! (just put the encodings
;		                             directly after the IRQ-table)

; a byte in the EEPROM has the following meaning
; 0b0x normal index into memory
; 0b100x direct pins access
; 0b101x pause (5 least-sign. bits are how many)
; 0b110x change color
; 0xFF   last

; some default values:

; in ~milli-seconds (max 255)
#ifndef POV_COLUMN_DURATION
#  define POV_COLUMN_DURATION 254
#endif

#ifndef POV_START_COLORS
#  define POV_START_COLORS 0b00011111
#endif

#ifndef POV_SPACE_NB_PAUSE
#  define POV_SPACE_NB_PAUSE 4
#endif

#ifndef POV_EEP_SIZE
#  define POV_EEP_SIZE 64
#endif

#ifndef POV_ENCODINGS_LOCATION
#  define POV_ENCODINGS_LOCATION DATA_ENC
#endif

#ifndef POV_TMP
#  ifdef TMP
#    define POV_TMP TMP
#  else
#    error "You must either define the TMP or POV_TMP register"
#  endif
#endif

#ifndef POV_TMP_IRQ
#  ifdef TMP
#    define POV_TMP_IRQ TMP_IRQ
#  else
#    error "You must either define the TMP_IRQ or POV_TMP_IRQ register"
#  endif
#endif


.macro PovGetChar RdEepChar RdEepCtr
	INFO("PovGetChar")
193100:	;GET_CHAR_START:
	LoadFromEeprom \RdEepCtr
	GetEepromChar \RdEepChar

	; end-marker?
	mov	POV_TMP, \RdEepChar
	cpi	POV_TMP, 0xFF
	brne	193101f
	clr	\RdEepCtr
	rjmp	193100b

193101:	;NOT_END_MARKER:
	inc	\RdEepCtr
	ldi	POV_TMP, POV_EEP_SIZE
	cp	\RdEepCtr, POV_TMP
	brne	193102f
	clr	\RdEepCtr
193102:	;END_GET_CHAR:
.endm

.macro PovPut4RdIntoZ Rd
	INFO("PovPut4RdIntoZ")
	clr	ZH
	mov	ZL, \Rd
	; * 2
	add	ZL, ZL
	rol	ZH			; rotate left through carry
	; * 2 -> = * 4
	add	ZL, ZL
	rol	ZH
.endm

.macro PovOutputRd Rd RdColors RdMilliSecsHigh RdMilliSecsLow
	INFO("PovOutputRd")
	mov	POV_TMP, \Rd
	andi	POV_TMP, 0b00011111
	out	_SFR_IO_ADDR(DDRB), POV_TMP	; Data Direction
	and	POV_TMP, \RdColors		; Read ports should be 0 (for Hi-Z)
	out	_SFR_IO_ADDR(PORTB), POV_TMP
	Ldi16 \RdMilliSecsHigh, \RdMilliSecsLow, POV_COLUMN_DURATION
	SleepMilliSecs16 \RdMilliSecsHigh, \RdMilliSecsLow
.endm

.macro PovTreatChar RdEepChar RdPauseCtr RdOutput RdColors RdMemoryChar RdMilliSecsHigh RdMilliSecsLow
	INFO("PovTreatChar")
	
	bst	\RdEepChar, 7
	brtc	POV_PTR_INPUT
POV_DIRECT_INPUT:
	mov	\RdMemoryChar, \RdEepChar

	; does \RdMemoryChar start with 0b101? (pause nb)
	mov	POV_TMP, \RdMemoryChar
	andi	POV_TMP, 0b11100000
	cpi	POV_TMP, 0b10100000
	brne	POV_NOT_PAUSE_NB
	mov	\RdPauseCtr, \RdMemoryChar
	andi	\RdPauseCtr, 0b00011111
	; bit 5 is already set because \RdMemoryChar starts with 0b101
	; --> LAST bit is set
	rjmp	POV_OUTPUT_PAUSE

POV_NOT_PAUSE_NB:
	; make any direct operation LAST
	ldi	POV_TMP, 0b00011111
	and	\RdMemoryChar, POV_TMP
	ldi	POV_TMP, (1 << 5)		; bit 5 is LAST bit
	or	\RdMemoryChar, POV_TMP

	rjmp	POV_ANALYSE_CHAR

POV_PTR_INPUT:
	mov	POV_TMP, \RdEepChar
	andi	POV_TMP, 0b01111111

	; put start-ptr into Z
	PovPut4RdIntoZ POV_TMP
	AddKToZ POV_ENCODINGS_LOCATION

POV_COLUMN_LOOP:
	.ifc \RdMemoryChar, r0
		lpm	; load into r0
	.else
		; make sure, that r0 won't be change inside an interrupt
		.warning "PovTreatChar: RdMemoryChar != r0, using slower version"
		cli
		XORSwap r0, \RdMemoryChar
		lpm
		XORSwap r0, \RdMemoryChar
		sei
	.endif

	IncZ

	; set SPACE_CTR
	clr	\RdPauseCtr
	bst	\RdMemoryChar, 6
	brtc	POV_NO_PAUSE
	inc	\RdPauseCtr
	rjmp	POV_NO_SPACE
POV_NO_PAUSE:
	sbrc	\RdMemoryChar, 7
	ldi	\RdPauseCtr, POV_SPACE_NB_PAUSE
POV_NO_SPACE:

POV_ANALYSE_CHAR:
	mov	POV_TMP, \RdMemoryChar
	andi	POV_TMP, 0b11000000
	cpi	POV_TMP, 0b11000000
	breq	POV_CHANGE_COLOR

	; pins:
	mov	\RdOutput, \RdMemoryChar
	PovOutputRd \RdOutput, \RdColors, \RdMilliSecsHigh, \RdMilliSecsLow
	
POV_OUTPUT_PAUSE:
	tst	\RdPauseCtr
	breq	POV_NO_PAUSE_OUTPUT
	dec	\RdPauseCtr
	clr	\RdOutput
	PovOutputRd \RdOutput, \RdColors, \RdMilliSecsHigh, \RdMilliSecsLow
	rjmp	POV_OUTPUT_PAUSE
POV_NO_PAUSE_OUTPUT:

POV_TEST_END_LOOP:
	; should we loop?
	sbrs	\RdMemoryChar, 5
	rjmp	POV_COLUMN_LOOP

	rjmp	POV_END_TREAT_CHAR

POV_CHANGE_COLOR:
	mov	POV_TMP, \RdMemoryChar
	andi	POV_TMP, 0b00011111
	mov	\RdColors, POV_TMP
	rjmp	POV_TEST_END_LOOP

POV_END_TREAT_CHAR:
.endm

.macro PovGetAndTreatChar RdEepChar RdEepCtr RdPauseCtr RdOutput RdColors RdMemoryChar RdMilliSecsHigh RdMilliSecsLow
	INFO("PovGetAndTreatChar")
	.ifndef Pov_Initialized
	  .error "PovGetAndTreatChar: you need to call PovInitialize first"
	.endif
	PovGetChar \RdEepChar, \RdEepCtr
	PovTreatChar \RdEepChar, \RdPauseCtr, \RdOutput, \RdColors, \RdMemoryChar, \RdMilliSecsHigh, \RdMilliSecsLow
.endm

.macro PovInitialize RdEepCtr RdColors
	INFO("PovInitialize")
	.equ Pov_Initialized, 1

	clr	\RdEepCtr

	ldi	POV_TMP, POV_START_COLORS
	mov	\RdColors, POV_TMP

	SleepInitialize
.endm

