NAME	= pov-audioIn
CC	= avr-gcc -D DEBUG -I. -Wall -mmcu=attiny12 -nostdlib -Wa,-warn,-g -Wl,-Map,$(NAME).map
#CC	= avr-gcc -I. -Wall -mmcu=attiny12 -nostdlib -Wa,-warn,-g,-alhms,-L -Wl,-Map,$(NAME).map
OBJCOPY	= avr-objcopy
OBJDUMP	= avr-objdump

#FLASH_COMMAND	= avrdude -pt12 -c bsd2
FLASH_COMMAND	= avrdude -pt12 -c usbasp
FLASH		= $(FLASH_COMMAND) -U flash:w:$(NAME).hex -y
FLASH_EEP	= $(FLASH_COMMAND) -U eeprom:w:$(NAME).eep -y

# get the calibration byte
#avrdude -pt12 -D -c usbasp -U cal:r:cal.tmp:r

HEX_EEPROM_FLAGS = -j .eeprom
HEX_EEPROM_FLAGS += --set-section-flags=.eeprom="alloc,load"
HEX_EEPROM_FLAGS += --change-section-lma .eeprom=0 --no-change-warnings

all:		$(NAME).hex $(NAME).eep

tests:		tests/pov.hex tests/pov.eep

clean:
	rm -f $(NAME).o $(NAME).bin *.sch~ gschem.log *.S~ *.hex *.map *.eep

clobber:	clean
	rm -f $(NAME).hex $(NAME).ps $(NAME).eep

flash:		$(NAME).hex
	$(FLASH)

flashEep:	$(NAME).eep
	$(FLASH_EEP)

%.hex:		%.bin
	$(OBJCOPY) -R eeprom -O ihex $< $@

%.eep:		%.bin
	$(OBJCOPY) $(HEX_EEPROM_FLAGS) -O ihex $< $@

%.bin:		%.o
	$(CC) $< -o $@

%.ps:		%.sch sch2ps
	sch2ps $<

