; vim:syntax=asm:
; using 123 as prefix for local labels

#pragma once

#ifndef DEBUG
#  define INFO(MESSAGE)
#else
#  define INFO(MESSAGE) .warning MESSAGE
#endif

#define PORF 0

.macro IfLtR16 Rd Flag
	INFO("IfLtR16")
	.equ \Flag, 0
	.ifc \Rd, r0
	  .equ \Flag, 1
	.endif
	.ifc \Rd, r1
	  .equ \Flag, 1
	.endif
	.ifc \Rd, r2
	  .equ \Flag, 1
	.endif
	.ifc \Rd, r3
	  .equ \Flag, 1
	.endif
	.ifc \Rd, r4
	  .equ \Flag, 1
	.endif
	.ifc \Rd, r5
	  .equ \Flag, 1
	.endif
	.ifc \Rd, r6
	  .equ \Flag, 1
	.endif
	.ifc \Rd, r7
	  .equ \Flag, 1
	.endif
	.ifc \Rd, r8
	  .equ \Flag, 1
	.endif
	.ifc \Rd, r9
	  .equ \Flag, 1
	.endif
	.ifc \Rd, r10
	  .equ \Flag, 1
	.endif
	.ifc \Rd, r11
	  .equ \Flag, 1
	.endif
	.ifc \Rd, r12
	  .equ \Flag, 1
	.endif
	.ifc \Rd, r13
	  .equ \Flag, 1
	.endif
	.ifc \Rd, r14
	  .equ \Flag, 1
	.endif
	.ifc \Rd, 'r15'
	  .equ \Flag, 1
	.endif
.endm

