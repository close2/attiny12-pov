; vim:syntax=asm:
; using 373 as prefix for local labels

; defines for the test-skeleton

#pragma once

#include "internal.h"
#include <avr/io.h>

#ifndef TESTS_IRQ_INT0
#  define TESTS_IRQ_INT0 reti
#endif

#ifndef TESTS_IRQ_IO_PINS
#  define TESTS_IRQ_IO_PINS reti
#endif

#ifndef TESTS_IRQ_TIMER0_OVF0
#  define TESTS_IRQ_TIMER0_OVF0 reti
#endif

#ifndef TESTS_IRQ_EE_RDY
#  define TESTS_IRQ_EE_RDY reti
#endif

#ifndef TESTS_IRQ_ANA_COMP
#  define TESTS_IRQ_ANA_COMP reti
#endif

#ifndef TESTS_RESET
#  define TESTS_RESET
#endif

#ifndef TESTS_MAIN
#  define TESTS_MAIN
#endif

#ifndef TESTS_MAIN_LOOP
#  define TESTS_MAIN_LOOP
#endif

#ifndef TESTS_EEPROM_DATA1
#  define TESTS_EEPROM_DATA1 0x0F
#endif

#ifndef TESTS_EEPROM_DATA2
#  define TESTS_EEPROM_DATA2 0x0E
#endif


; Output is 11100 if successful and 00100 if we had an error
; the lower bits are left 0 (the direction is not changed neither)
; --> you can use ShowResult when using anaComp
; Success: RdResult == 1; Error: RdResult == 0
.macro ShowResult RdResult
	INFO("ShowResult")
	sbi	_SFR_IO_ADDR(DDRB), DDB2
	sbi	_SFR_IO_ADDR(DDRB), DDB3
	sbi	_SFR_IO_ADDR(DDRB), DDB4

	sbrc	\RdResult, 0
	rjmp	373100f	; Success

	sbi	_SFR_IO_ADDR(PORTB), PB2
	cbi	_SFR_IO_ADDR(PORTB), PB3
	cbi	_SFR_IO_ADDR(PORTB), PB4
	rjmp	373101f	; Error

373100:
	sbi	_SFR_IO_ADDR(PORTB), PB2
	sbi	_SFR_IO_ADDR(PORTB), PB3
	sbi	_SFR_IO_ADDR(PORTB), PB4
373101:
.endm

