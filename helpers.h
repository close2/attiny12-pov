; vim:syntax=asm:
; using 143 as prefix for local labels

#pragma once

#include <avr/io.h>
#include "internal.h"
#include "16bit.h"

; define some missing constants
#ifndef PORF
#  define PORF	0
#endif
#ifndef EXTRF
#  define EXTRF	1
#endif
#ifndef BORF
#  define BORF	2
#endif
#ifndef WDRF
#  define WDRF	3
#endif

#ifndef HELPERS_TMP
#  ifdef TMP
#    define HELPERS_TMP TMP
#  else
#    error "You must either define the TMP or HELPERS_TMP register"
#  endif
#endif

; if clock is 1.2MHz and Prescale is set to 1024,
; 1 s == 1172 IRQs
#ifndef HELPERS_PRESCALE1024_1S
#  define HELPERS_PRESCALE1024_1S 1172
#endif

; if clock is 1.2MHz and Prescale is set to 1024,
; 1 s == 150 IRQs --> 256 - 150 = 106
#ifndef HELPERS_PRESCALE8_1MS
#  define HELPERS_PRESCALE8_1MS 150
#endif

.macro AddCarry Reg
	INFO("AddCarry")
	brcc	143100f
	inc	\Reg
143100:
.endm

.macro BackupSREG RdBackupTo
	INFO("BackupSREG")
	in \RdBackupTo, _SFR_IO_ADDR(SREG)
.endm
.macro RestoreSREG RdRestoreFrom
	INFO("RestoreSREG")
	out _SFR_IO_ADDR(SREG), \RdRestoreFrom
.endm

; ################### ANALOG COMPARATOR ######################
; it is probably possible to combine at least 2 actions below
; kMode==__analog__comp__toggle__ irq on toggle
; kMode==__analog__comp__fall__ irq on falling
; kMode==__analog__comp__raise__ irq on raising
.macro AnalogCompOn kMode=__analog_comp_raise__
	INFO("AnalogCompOn")
	; deactivate interrupts
	cbi	_SFR_IO_ADDR(ACSR), ACIE
	; turn off power disable
	cbi	_SFR_IO_ADDR(ACSR), ACD
  .ifc \kMode, __analog__comp__toggle__
	; interrupt on toggle
	cbi	_SFR_IO_ADDR(ACSR), ACIS0
	cbi	_SFR_IO_ADDR(ACSR), ACIS1
  .endif
  .ifc \kMode, __analog__comp__fall__
	; interrupt on Falling Output Edge
	cbi	_SFR_IO_ADDR(ACSR), ACIS0
	sbi	_SFR_IO_ADDR(ACSR), ACIS1
  .endif
  .ifc \kMode, __analog__comp__raise__
	; interrupt on Rising Output Edge
	sbi	_SFR_IO_ADDR(ACSR), ACIS0
	sbi	_SFR_IO_ADDR(ACSR), ACIS1
  .endif
	; activate interrupts
	sbi	_SFR_IO_ADDR(ACSR), ACIE
.endm

.macro AnalogCompOff
	INFO("AnalogCompOff")
	; deactivate interrupts
	cbi	_SFR_IO_ADDR(ACSR), ACIE
	; enable power disable
	sbi	_SFR_IO_ADDR(ACSR), ACD
.endm

.macro GetAnalogCompOutput RdOutput
	INFO("GetAnalogCompOutput")
	clr	\RdOutput
	in	\RdOutput, ACSR
	; could use bst, but don't want to do this inside macro
	sbrc	\RdOutput, ACO
	rjmp	143501f

	clr	\RdOutput
	rjmp	143502f
143501:
	clr	\RdOutput
	inc	\RdOutput
143502:
.endm

; use the internal 1.22 (+/- 0.05)V ref-voltage
.macro AnalogCompIntRefV
	INFO("AnalogCompIntRefV")
	sbi	_SFR_IO_ADDR(ACSR), AINBG
	cbi	_SFR_IO_ADDR(DDRB), PB1		;AIN1
	cbi	_SFR_IO_ADDR(PORTB), PB1	;AIN1
	; insert some statements to give the internal RefV
	; some time to start
	ldi	HELPERS_TMP, 10
143400:
	dec	HELPERS_TMP
	brne	143400b
.endm

.macro AnalogCompPinComp
	INFO("AnalogCompPinComp")
	cbi	_SFR_IO_ADDR(ACSR), AINBG
	cbi	_SFR_IO_ADDR(DDRB), PB0		;AIN0
	cbi	_SFR_IO_ADDR(PORTB), PB0	;AIN0
	cbi	_SFR_IO_ADDR(DDRB), PB1		;AIN1
	cbi	_SFR_IO_ADDR(PORTB), PB1	;AIN1
.endm

; ################### COUNTER / CLOCK ######################
.macro SetPrescale8
	INFO("SetPrescale8")
	ldi	HELPERS_TMP, 0b00000010
	out	_SFR_IO_ADDR(TCCR0), HELPERS_TMP
.endm

.macro SetPrescale1024
	INFO("SetPrescale1024")
        ; init counter, clock is ~1.2MHz --> 256*1024/1.2MHz =~ 0.2s
        ldi     HELPERS_TMP, 0b00000101         ; select Prescale to CK/1024
        out     _SFR_IO_ADDR(TCCR0), HELPERS_TMP; Timer/Counter0 Control Register
.endm

.macro TimerCounterOn
	INFO("TimerCounterOn")
        ; enable timer/counter interrupt
        ldi     HELPERS_TMP, (1 << TOIE0)       ; Timer/Counter0 Overflow Interrupt En.
        out     _SFR_IO_ADDR(TIMSK), HELPERS_TMP; Timer/Counter Interrupt Mask
.endm

.macro TimerCounterOff
	INFO("TimerCounterOff")
	; disable timer/counter interrupt
	clr	HELPERS_TMP
	out	_SFR_IO_ADDR(TIMSK), HELPERS_TMP
.endm

.macro SetCounterTicks RdInitialRemainingTicks
	INFO("SetCounterTicks")
        out     _SFR_IO_ADDR(TCNT0), \RdInitialRemainingTicks; Timer Counter 0
.endm
.macro SetCounterTicks_k kInitRemainingTicks
	INFO("SetCounterTicks")
        ldi     HELPERS_TMP, \kInitRemainingTicks
        out     _SFR_IO_ADDR(TCNT0), HELPERS_TMP; Timer Counter 0
.endm

; GetCounterTicks also allows TMP!
.macro GetCounterTicks RdTmp
	INFO("GetCounterTicks")
	in	\RdTmp, _SFR_IO_ADDR(TCNT0)
.endm

; __idle__ or __power_down__
.macro SleepEnable kMode=__idle__
	INFO("SleepEnable")
	; enable sleep mode
	in	HELPERS_TMP, _SFR_IO_ADDR(MCUCR); read MCUCR
	ori	HELPERS_TMP, (1 << SE)		; enable sleep
      .ifc \kMode, __idle__
	andi	HELPERS_TMP, ~(1 << SM)		; set sleep-mode to Idle
      .else
	; assume __power_down__
	ori	HELPERS_TMP, (1 << SM)		; set sleep-mode to Power down
      .endif
	out	_SFR_IO_ADDR(MCUCR), HELPERS_TMP; MCU Control Register
.endm

.macro SleepModePowerDown
	INFO("SleepModePowerDown")
	in	HELPERS_TMP, _SFR_IO_ADDR(MCUCR); read MCUCR
	ori	HELPERS_TMP, (1 << SM)		; set sleep-mode to Idle
	out	_SFR_IO_ADDR(MCUCR), HELPERS_TMP; MCU Control Register
.endm

.macro SleepModeIdle
	INFO("SleepModeIdle")
	in	HELPERS_TMP, _SFR_IO_ADDR(MCUCR); read MCUCR
	andi	HELPERS_TMP, ~(1 << SM)		; set sleep-mode to Idle
	out	_SFR_IO_ADDR(MCUCR), HELPERS_TMP; MCU Control Register
.endm

.macro SleepDisable
	INFO("SleepDisable")
	; disable sleep mode
	in	HELPERS_TMP, _SFR_IO_ADDR(MCUCR); read MCUCR
	andi	HELPERS_TMP, ~(1 << SE)		; disable sleep
	out	_SFR_IO_ADDR(MCUCR), HELPERS_TMP; MCU Control Register
.endm
; modifies RdMilliSecs{High, Low}
; every millisecond is clocked on its own
; --> we can however never be sure, that we wake from the sleep
; because of the overrun of the counter and therefore check
; the counter value.  If it is below HELPERS_PRESCALE8_1MS there has been an
; overrun (very small possibility that this check is executed too late
; because other interrupts are called all the time)
; modes: __idle__ or __power_down__
.macro SleepInitialize kMode=__idle__
	INFO("SleepInitialize")
	.equ Helpers_SleepInitialized, 1
	SetPrescale8
	SleepEnable \kMode
	TimerCounterOn
.endm

; TODO extract arbitrary 160
.macro SleepUnit16Sub kCounterUnits
	INFO("SleepUnit16Sub")
	.if \kCounterUnits < 160
	  SetCounterTicks_k 255 - \kCounterUnits
	.else
	  SetCounterTicks_k 255 - 160
	.endif
143650:	; sleep-interrupt loop
	sei	; enable global interrupt
	sleep	; the sei and sleep must not be separated
	cli	; disable global interrupt
	; check if current ticks are below initial value
	GetCounterTicks	HELPERS_TMP
	.if \kCounterUnits < 160
	  cpi	HELPERS_TMP, 255 - \kCounterUnits
	.else
	  cpi	HELPERS_TMP, 255 - 160
	.endif
	brsh	143650b	; branch if same or higher

	.if \kCounterUnits >= 160
	  .equ kNewCounterUnits, \kCounterUnits - 160
	  SleepUnit16Sub kNewCounterUnits
	.endif
.endm

.macro SleepUnit16 RdUnitHigh RdUnitLow kCounterUnits
	INFO("SleepUnit16")
	clt
	brid	143700f
	set

143700:	; Loop
	Tst16	\RdUnitHigh, \RdUnitLow
	;breq	143795f	; need to use rjmp because SleepUnit16Sub might be large!
	brne	143740f
	rjmp	143795f
143740:
	SleepUnit16Sub \kCounterUnits

	; ok we had an overrun
	Dec16	\RdUnitHigh, \RdUnitLow, HELPERS_TMP, 1
	rjmp	143700b
143795:	; End-Loop	

	brtc	143796f
	sei
143796:
.endm

; SleepMilliSecs expects prescale to be 8
.macro SleepMilliSecs16 RdMilliSecsHigh RdMilliSecsLow
	INFO("SleepMilliSecs16")
	.ifndef Helpers_SleepInitialized
	  .error "SleepMilliSecs16: You need to call initialize first"
	.endif
	SleepUnit16 \RdMilliSecsHigh \RdMilliSecsLow HELPERS_PRESCALE8_1MS
.endm

; SleepSecs16 sets prescale to 1024! don't use both Sleeps at the same time
; (it's save however to call one after the other)  (this is only an issue with irqs)
.macro SleepSecs16 RdSecsHigh RdSecsLow
	INFO("SleepSecs16")
	.ifndef Helpers_SleepInitialized
	  .error "SleepSecs16: You need to call initialize first"
	.endif
	; 1 second is long enought that setting prescale doesn't matter
	SetPrescale1024
	SleepUnit16 \RdSecsHigh \RdSecsLow HELPERS_PRESCALE1024_1S
	SetPrescale8
.endm

.macro IncZ
	INFO("IncZ")
	subi	ZL, 0xFF		; Z + 1 == Z - (-1)
	sbci	ZH, 0xFF
.endm

.macro AddKToZ K
	INFO("AddKToZ")
	ldi	HELPERS_TMP, \K
	add	ZL, HELPERS_TMP
	brcc	143800f
	inc	ZH
143800:
.endm

.macro GetEepromAddress RdAddress
	INFO("GetEepromAddress")
	in	\RdAddress, _SFR_IO_ADDR(EEAR)
.endm

.macro SetEepromAddress RdAddress
	INFO("SetEepromAddress")
143200:	; wait for EEWE==0
	in	HELPERS_TMP, _SFR_IO_ADDR(EECR)
	andi	HELPERS_TMP, (1 << EEWE)
	brne	143200b
	out	_SFR_IO_ADDR(EEAR), \RdAddress
.endm

.macro LoadFromEeprom RdAddress
	INFO("LoadFromEeprom")
	SetEepromAddress \RdAddress
	sbi	_SFR_IO_ADDR(EECR), EERE	; load from eeprom (EEPROM Read En.)
.endm

.macro GetEepromChar Rd                                                             
	INFO("GetEepromChar")
	in	\Rd, _SFR_IO_ADDR(EEDR)         ; EEPROM Data Register
.endm                                                                               

; default-value for RdAddress implies current Eeprom-address
.macro WriteEeprom RdByte RdAddress=r9999
	INFO("WriteEeprom")
	; busy wait for EEWE to become zero
143300:	; wait for EEWE==0
	in	HELPERS_TMP, _SFR_IO_ADDR(EECR)
	andi	HELPERS_TMP, (1 << EEWE)
	brne	143300b

  .ifnc \RdAddress, r9999
	out	_SFR_IO_ADDR(EEAR), \RdAddress
  .endif
	out	_SFR_IO_ADDR(EEDR), \RdByte
	in	HELPERS_TMP, _SFR_IO_ADDR(EECR)
	; EEWE must be zero, as we busy-waited for this state
	; as setting EEWE must be within 4 clock cycles temp. disable IRQs
	; we use the highest bit of HELPERS_TMP to remember if IRQs were enabled
	andi	HELPERS_TMP, 0b01111111
	brid	143301f		; branch if interrupt disabled
	cli	; disable IRQs
	ori	HELPERS_TMP, 0b10000000
143301:
	ori	HELPERS_TMP, (1 << EEMWE)
	; don't need to set EEWE correctly in HELPERS_TMP (must be 0 anyway)
	out	_SFR_IO_ADDR(EECR), HELPERS_TMP
	; datasheet says we need to set EEWE to 0 in the same cycle
	; so the next line shouldn't work
	;sbi	_SFR_IO_ADDR(EECR), EEMWE

	sbi	_SFR_IO_ADDR(EECR), EEWE
	sbrc	HELPERS_TMP, 7
	sei	; reenable IRQs
.endm

.macro IncEepromAddress kCompareWith=-1
	INFO("IncEepromAddress")
	in	HELPERS_TMP, _SFR_IO_ADDR(EEAR)
	inc	HELPERS_TMP
	out	_SFR_IO_ADDR(EEAR), HELPERS_TMP
  .if \kCompareWith >= 0
	cpi	HELPERS_TMP, \kCompareWith
  .endif
.endm

.macro ClearEepromAddress
	INFO("ClearEepromAddress")
	clr	HELPERS_TMP
	out	_SFR_IO_ADDR(EEAR), HELPERS_TMP
.endm

; probably only useful to quickly find the name of the
; register (and to be sure, that there isn't anything
; else to do)
.macro OutputIOPort Rd
	INFO("OutputIOPort")
	out	_SFR_IO_ADDR(PORTB), \Rd
.endm

.macro SetIOPortDirections Rd
	INFO("SetIOPortDirections")
	out	_SFR_IO_ADDR(DDRB), \Rd
.endm

.macro BranchAfterReset kLabelExtReset=__no__jump__ kLabelPowerOn=__no__jump__ kLabelWatchdog=__no__jump__ kLabelBrownOut=__no__jump__
	INFO("BranchAfterReset")
	; find out, if Power-on Reset or External Pin Reset
	in	HELPERS_TMP, _SFR_IO_ADDR(MCUSR)
	; shift values to left, so that we can set MCUSR to 0 but don't lose
	; the original values
	lsl	HELPERS_TMP
	lsl	HELPERS_TMP
	lsl	HELPERS_TMP
	lsl	HELPERS_TMP
	out	_SFR_IO_ADDR(MCUSR), HELPERS_TMP
	sbrc	HELPERS_TMP, (PORF + 4)
      .ifnc \kLabelPowerOn, __no__jump__
	rjmp	\kLabelPowerOn
      .else
        rjmp	143901f
      .endif
      .ifnc \kLabelExtReset, __no__jump__
	sbrc	HELPERS_TMP, (EXTRF + 4)
	rjmp	\kLabelExtReset
      .endif
      .ifnc \kLabelWatchdog, __no__jump__
	sbrc	HELPERS_TMP, (WDRF + 4)
	rjmp	\kLabelWatchdog
      .endif
      .ifnc \kLabelBrownOut, __no__jump__
	sbrc	HELPERS_TMP, (BORF + 4)
	rjmp	\kLabelBrownOut
      .endif
143901:
.endm

.macro OutputValueSerial_Sub RdValue RdId RdDurationHigh RdDurationLow kDuration kPortValueSep kPortId kPortValue kBit
	INFO("OutputValueSerial_Sub")

    .if \kBit == 0
	sbi	_SFR_IO_ADDR(PORTB), \kPortValueSep
    .endif
    .if \kBit == 2
	cbi	_SFR_IO_ADDR(PORTB), \kPortValueSep
    .endif
    .if \kBit == 3
	sbi	_SFR_IO_ADDR(PORTB), \kPortValueSep
    .endif
    .if \kBit == 4
	cbi	_SFR_IO_ADDR(PORTB), \kPortValueSep
    .endif
    .if \kBit == 5
	sbi	_SFR_IO_ADDR(PORTB), \kPortValueSep
    .endif
    .if \kBit == 6
	cbi	_SFR_IO_ADDR(PORTB), \kPortValueSep
    .endif

	cbi	_SFR_IO_ADDR(PORTB), \kPortId
	sbrc	\RdId, \kBit
	sbi	_SFR_IO_ADDR(PORTB), \kPortId

	cbi	_SFR_IO_ADDR(PORTB), \kPortValue
	sbrc	\RdValue, \kBit
	sbi	_SFR_IO_ADDR(PORTB), \kPortValue

	Ldi16 \RdDurationHigh, \RdDurationLow, \kDuration
	SleepMilliSecs16 \RdDurationHigh, \RdDurationLow
.endm

.macro OutputValueSerial RdValue RdId RdDurationHigh RdDurationLow kDuration=1000 kPortValueSep=2 kPortId=3 kPortValue=4
	INFO("OutputValueSerial")
	sbi	_SFR_IO_ADDR(DDRB), \kPortValueSep
	sbi	_SFR_IO_ADDR(DDRB), \kPortId
	sbi	_SFR_IO_ADDR(DDRB), \kPortValue

	OutputValueSerial_Sub \RdValue, \RdId, \RdDurationHigh, \RdDurationLow, \kDuration, \kPortValueSep, \kPortId, \kPortValue, 0
	OutputValueSerial_Sub \RdValue, \RdId, \RdDurationHigh, \RdDurationLow, \kDuration, \kPortValueSep, \kPortId, \kPortValue, 1
	OutputValueSerial_Sub \RdValue, \RdId, \RdDurationHigh, \RdDurationLow, \kDuration, \kPortValueSep, \kPortId, \kPortValue, 2
	OutputValueSerial_Sub \RdValue, \RdId, \RdDurationHigh, \RdDurationLow, \kDuration, \kPortValueSep, \kPortId, \kPortValue, 3
	OutputValueSerial_Sub \RdValue, \RdId, \RdDurationHigh, \RdDurationLow, \kDuration, \kPortValueSep, \kPortId, \kPortValue, 4
	OutputValueSerial_Sub \RdValue, \RdId, \RdDurationHigh, \RdDurationLow, \kDuration, \kPortValueSep, \kPortId, \kPortValue, 5
	OutputValueSerial_Sub \RdValue, \RdId, \RdDurationHigh, \RdDurationLow, \kDuration, \kPortValueSep, \kPortId, \kPortValue, 6
	OutputValueSerial_Sub \RdValue, \RdId, \RdDurationHigh, \RdDurationLow, \kDuration, \kPortValueSep, \kPortId, \kPortValue, 7
.endm

